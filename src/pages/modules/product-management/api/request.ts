import request from "../../../../utils/index"


interface DataType {
    size: number,
    current: number
}

interface GurdType {
    size: number,
    current: number,
    propName: string
}

//分组管理列表数据
export const getList = (params: DataType) => request.get({ url: '/api/prod/prodTag/page', params })

//增加分组列表
export const editList = (data: any) => request.post({ url: '/api/prod/prodTag', data })

//修改回显
export const updataList = (data: any) => request.get({ url: `/api/prod/prodTag/info/${data}` })

//提交修改
export const updataSubmin = (data: any) => request.put({ url: '/api/prod/prodTag', data })

//分组管理删除
export const deleteGroup = (id: number) => request.delete({ url: `/api/prod/prodTag/${id}` })

//分组管理搜索
export const searchGroup = (params: any) => request.get({ url: '/api/prod/prodTag/page', params })

//分类管理  数据修改回显  /prod/category/info
export const classifyCategory = (data: number) => request.get({ url: `/api/prod/category/info/${data}` })

//分类管理  数据修改提交
export const updataCategory = (data: any) => request.put({ url: '/api/prod/category', data })

//产品管理
export const getProductList = (params: DataType) => request.get({ url: '/api/prod/prod/page', params })

//规格管理列表数据
export const getGurdList = (params: DataType) => request.get({ url: '/api/prod/spec/page', params })

//规格搜索
export const searchGurd = (params: GurdType) => request.get({ url: '/api/prod/spec/page', params })

//规格添加
export const addGurd = (data: any) => request.post({ url: '/api/prod/spec', data })

//规格删除
export const deleteGurd = (data: any) => request.delete({ url: `/api/prod/spec/${data[0]}` })

//规格修改
export const updataCurd = (data: any) => request.put({ url: '/api/prod/spec', data })

//分类管理
export const categoryList = () => request.get({ url: '/api/prod/category/table' })
