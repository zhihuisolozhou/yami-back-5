
import { createRouter, createWebHistory, } from 'vue-router'

const routers = [
    {
        path: '/index/home',
        name: '首页',
        component: () => import(`../../pages/modules/home/index.vue`)
    },

    {
        path: '/index/product-management',
        name: '产品管理',
        children: [
            {
                path: '/prod/prodTag',
                name: 'prod:prodTag',
                component: () => import(`../../pages/modules/product-management/grouping/index.vue`),
            },
            {
                path: '/prod/prodList',
                name: 'prod:prodList',
                component: () => import(`../../pages/modules/product-management/product/index.vue`),
            },
            {
                path: '/prod/category',
                name: 'prod:category',
                component: () => import(`../../pages/modules/product-management/classify/index.vue`),
            },
            {
                path: '/prod/prodComm',
                name: 'prod:prodComm',
                component: () => import(`../../pages/modules/product-management/comment/index.vue`),
            },
            {
                path: '/prod/spec',
                name: 'prod:spec',
                component: () => import(`../../pages/modules/product-management/specification/index.vue`),
            }
        ]
    },
    {
        path: '/index/Store-management',
        name: '门店管理',
        children: [
            {
                path: '/shop/notice',
                name: '公告管理',
                component: () => import(`../../pages/modules/Store-management/announcement/index.vue`),
            },
            {
                path: '/shop/hotSearch',
                name: '热搜管理',
                component: () => import(`../../pages/modules/Store-management/top-search/index.vue`),
            },
            {
                path: '/admin/indexImg',
                name: '轮播图管理',
                component: () => import(`../../pages/modules/Store-management/slideshow/index.vue`),
            },
            {
                path: '/shop/transport',
                name: '运费模板',
                component: () => import(`../../pages/modules/Store-management/freight-charge/index.vue`),
            },
            {
                path: '/shop/pickAddr',
                name: '自提点管理',
                component: () => import(`../../pages/modules/Store-management/self-pick-up-site/index.vue`),
            }
        ]
    },
    {
        path: '/index/member-management',
        name: '会员管理',
        children: [
            {
                path: '/user/user',
                name: '会员管理',
                component: () => import(`../../pages/modules/member-management/member/index.vue`),
            },

        ]
    }, {
        path: '/index/order-management',
        name: '订单管理',
        children: [
            {
                path: '/order/order',
                name: '订单管理',
                component: () => import(`../../pages/modules/order-management/order-form/index.vue`),
            },

        ]
    },
    {
        path: '/index/system-management',
        name: '系统管理',
        children: [
            {
                path: '/sys/area',
                name: '地址管理',
                component: () => import(`../../pages/modules/system-management/location/index.vue`),
            },
            {
                path: '/sys/user',
                name: '管理员列表',
                component: () => import(`../../pages/modules/system-management/Administrator/index.vue`),
            },
            {
                path: '/sys/role',
                name: '角色管理',
                component: () => import(`../../pages/modules/system-management/role/index.vue`),
            },
            {
                path: '/sys/menu',
                name: '菜单管理',
                component: () => import(`../../pages/modules/system-management/menu/index.vue`),
            },
            {
                path: '/sys/schedule',
                name: '定时任务',
                component: () => import(`../../pages/modules/system-management/timed-task/index.vue`),
            },
            {
                path: '/sys/config',
                name: '参数管理',
                component: () => import(`../../pages/modules/system-management/parameter/index.vue`),
            },
            {
                path: '/sys/log',
                name: '系统日志',
                component: () => import(`../../pages/modules/system-management/system-journal/index.vue`),
            }
        ]
    },

]
const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: "/",
            redirect: { path: "/index" },
        },
        {
            path: '/index',
            name: 'index',
            component: () => import(`../../pages/index.vue`),
            children: routers
        },

        {
            path: '/login',
            name: 'login',
            component: () => import(`../../pages/modules/login/login.vue`)
        }
    ]
})
export default router

