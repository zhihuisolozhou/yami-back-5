import request from '../utils/index';
//登录
export const _login = (parmas: any) => (request.post({ url: '/api/login?grant_type=admin', data: parmas }))
//获取用户信息
export const admins = () => request.get({ url: `/api/sys/user/info` })
//获取导航列表
export const menuData = () => request.get({ url: `/api/sys/menu/nav` })
//订单  分页
export const _order = (size: any, orderNumber: any, status: any) => request.get({ url: `/api/order/order/page?current=${size.current}&size=${size.size}&orderNumber=${orderNumber}&status=${status}`, })
//详情
export const Sonorder = (id: any) => request.get({ url: `/api/order/order/orderInfo/${id}` })
//会员
export const admin = (size: any, nickName: any, status: any) => request.get({ url: `/api/admin/user/page?current=${size.current}&size=${size.size}&nickName=${nickName}&status=${status}`, })
//会员编辑
export const put_admin = (parmas: any) => (request.put({ url: '/api/admin/user', data: parmas }))
//系统日志
export const member = (size: any, username: any, operation: any) => request.get({ url: `/api/sys/log/page?current=${size.current}&size=${size.size}&username=${username}&operation=${operation}`, })
//参数管理
export const parameter = (size: any, paramKey: any) => request.get({ url: `/api/sys/config/page?current=${size.current}&size=${size.size}&paramKey=${paramKey}`, })
//参数删除
export const parameter_delete = (data: any) => request.delete({ url: '/api/sys/config', data })
//参数添加
export const add_parameter = (parmas: any) => (request.post({ url: '/api/sys/config', data: parmas }))
//编辑添加
export const put_parameter = (parmas: any) => (request.put({ url: '/api/sys/config', data: parmas }))
//菜单
export const menuLIist = () => request.get({ url: `/api/sys/menu/table` })
//菜单添加
export const add_menuLIist = (parmas: any) => (request.post({ url: '/api/sys/menu', data: parmas }))
//菜单修改
export const put_menuLIist = (parmas: any) => (request.put({ url: '/api/sys/menu', data: parmas }))
//菜单删除
export const menuLIist_delete = (data: any) => request.delete({ url: `/api/sys/menu/${data}` })
//地址管理
export const address = () => request.get({ url: `/api/admin/area/list` })
