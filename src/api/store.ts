import request from "../utils";

interface _noticeType {
    current: number,
    size: number
}

interface _addType {
    title: string,
    status: number,
    isTop: number,
    content: string
}

// 公告
export const _addNotice = (data: _addType) => request.post({ url: '/api/shop/notice', data })

export const _deleteNotice = (id: any) => request.delete({ url: `/api/shop/notice/${id}` })

export const _editNotice = (data: any) => request.put({ url: '/api/shop/notice', data })

export const _notice = (params: _noticeType) => request.get({ url: `/api/shop/notice/page`, params })

export const _geteditNotice = (id: any) => request.get({ url: `/api/shop/notice/info/${id}` })


// 热搜
export const _addHotsearch = (data: any) => request.post({ url: '/api/admin/hotSearch', data })

export const _deleteHotsearch = (data: any) => request.delete({ url: `/api/admin/hotSearch`, data })

export const _editHotsearch = (data: any) => request.put({ url: '/api/admin/hotSearch', data })

export const _hotsearch = (params: _noticeType) => request.get({ url: `/api/admin/hotSearch/page`, params })

export const _geteditHot = (id: any) => request.get({ url: `/api/admin/hotSearch/info/${id}` })

// 自提点
export const _addSelf = (data: any) => request.post({ url: '/api/shop/pickAddr', data })

export const _deleteSelf = (data: any) => request.delete({ url: `/api/shop/pickAddr`, data })

export const _editSelf = (data: any) => request.put({ url: '/api/shop/pickAddr', data })

export const _self = (params: _noticeType) => request.get({ url: `/api/shop/pickAddr/page`, params })

export const _geteditSelf = (id: any) => request.get({ url: `/api/shop/pickAddr/info/${id}` })

export const _getarea = (pid: any) => request.get({ url: `/api/admin/area/listByPid?pid=${pid}` })

// 轮播图
export const _addSwiper = (data: any) => request.post({ url: '/api/admin/indexImg', data })

export const _deleteSwiper = (data: any) => request.delete({ url: `/api/admin/indexImg`, data })

export const _editSwiper = (data: any) => request.put({ url: '/api/admin/indexImg', data })

export const _swiper = (params: _noticeType) => request.get({ url: `/api/admin/indexImg/page`, params })

export const _getSwiper = (imgId: any) => request.get({ url: `/api/admin/indexImg/info/${imgId}` })

export const _pick = (params: _noticeType) => request.get({ url: `/api/prod/prod/page`, params })

// 上传
export const _uploadelement = (data: any) => request.post({ url: `/api/admin/file/upload/element`, data })

// 运费
export const _addFreight = (data: any) => request.post({ url: '/api/shop/transport', data })

export const _deleteFreight = (data: any) => request.delete({ url: `/api/shop/transport`, data })

export const _editFreight = (data: any) => request.put({ url: '/api/shop/transport', data })

export const _freight = (params: _noticeType) => request.get({ url: `/api/shop/transport/page`, params })

export const _geteditFreight = (id: any) => request.get({ url: `/api/shop/transport/info/${id}` })

export const _getList = () => request.get({ url: `/api/admin/area/list` })
