import request from "../utils";
//管理员列表
export const System_Administrator = (data: any) =>
    request.get({
        url: `/api/sys/user/page?current=${data.page}&size=${data.pageSize}&username=${data.usernames}`,
    })
export const Administator_Get_Info = (userId: any) =>
    request.get({
        url: `/api/sys/user/info/${userId}`
    })
export const Administator_Put_User = (data: any) => {
    request.put({
        url: `/api/sys/user`,
        data: {
            email: data.email,
            mobile: data.mobile,
            password: "",
            roleIdList: data.type,
            status: data.status,
            userId: data.userId,
            username: data.username
        }
    })
}
export const Administator_Post_User = (data: any) => {
    request.post({
        url: `/api/sys/user`,
        data: {
            email: data.email,
            mobile: data.mobile,
            password: "",
            roleIdList: data.type,
            status: data.status,
            userId: data.userId,
            username: data.username
        }
    })
}

export const System_Administrator_Get_Role = () =>
    request.get({
        url: `/api/sys/role/page`,
    })
export const Administator_Delete_User = (data: any) =>
    request.delete({
        url: `/api/sys/user`,
        data
    })
//角色管理
export const System_Get_Role = (page: number, pageSize: number, usernames: string) =>
    request.get({
        url: `/api/sys/role/page?current=${page}&size=${pageSize}&roleName=${usernames}`,
    })
export const Role_Get_Table = () =>
    request.get({
        url: `/api/sys/menu/table`
    })
export const Role_Delete_Sys = (data: any) =>
    request.delete({
        url: `/api/sys/role`,
        data
    })
//定时任务
export const Timed_Get_Schedule = (page: number, pageSize: number, beanName: string) =>
    request.get({
        url: `/api/sys/schedule/page?page=${page}&limit=${pageSize}&beanName=${beanName}`
    })
// /sys/scheduleLog/page?t=1667272703414&page=1&limit=10&jobId=
export const Timed_Get_ScheduleLog = (page: number, pageSize: number, jobId: string) =>
    request.get({
        url: `/api/sys/scheduleLog/page?page=${page}&limit=${pageSize}&jobId=${jobId}`
    })
export const Timed_Put_ScheduleLog = (data: any) => {
    request.put({
        url: `/api/sys/schedule`,
        data: ({
            beanName: data.name,
            cronExpression: data.date2,
            jobId: data.jobId,
            methodName: data.region,
            params: data.date1,
            remark: data.resource,
            status: data.status,
        })
    })
}
export const Timed_Post_Pause = (data: any) => {
    request.post({
        url: `/api/sys/schedule/pause`,
        data
    })
}
export const Timed_Post_Resume = (data: any) => {
    request.post({
        url: `/api/sys/schedule/resume`,
        data
    })
}
export const Timed_Post_Run = (data: any) => {
    request.post({
        url: `/api/sys/schedule/run`,
        data
    })
}
export const Timed_Post_Schedule = (data: any) => {
    request.post({
        url: `/api/sys/schedule`,
        data: { ...data }
    })
}