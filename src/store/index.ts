import useLoginStore from './module/login';

export default function useStore() {
    return {
        login: useLoginStore()
    }
}