import { defineStore } from 'pinia'
import { _login, admins, menuData } from '../../api/login'
import router from '../../router/module'
import { useRouter } from 'vue-router';
import cache from '../../utils/utils';
import { getRoutes } from '../../utils/permission';
const $router = useRouter();
const login = defineStore('login', {
    state() {
        return {
            token: cache.getCache('token') || '',
            authorities: cache.getCache('authorities') || [],
            userinfo: cache.getCache('userinfo') || {},
            MenuData: cache.getCache('menuData') || []
        }
    },
    actions: {
        async loginAction(payload: any) {
            const { data }: any = await _login(payload)
            cache.setCache('token', `${data.token_type}${data.access_token}`)
            cache.setCache('authorities', data.authorities)
            this.token = `${data.token_type}${data.access_token}`;
            //获取用户信息
            const useinfo: any = await admins();
            this.userinfo = useinfo.data
            cache.setCache('userinfo', useinfo.data)
            //获取导航列表
            const menuList: any = await menuData()
            cache.setCache('menuData', menuList.data.menuList)
            console.log(menuList, 'menuList');
            this.changeMenu(menuList.data.menuList)
            $router.push({ path: '/index' })
        },
        //导航菜单
        changeMenu(payload: any) {
            const router: any = getRoutes(payload)
            //动态添加到路由表里面
            //addRouters addRouter
            //addRouter 路由的api  是增添一级路由的  如果要添加二级路由一定要带上父路由地址
            router.forEach((item: any) => {
                router.addRoute('home', item);

            })
            console.log(router);

        }



    }
})
export default login


