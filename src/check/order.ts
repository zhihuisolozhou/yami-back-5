export interface ORDER {
    values: order
}
interface order {
    actualTotal: number
    addrOrderId: number
    cancelTime: string
    createTime: string
    deleteStatus: number
    dvyFlowId: string
    dvyId: any
    dvyTime: any
    dvyType: any
    finallyTime: any
    freightAmount: number
    isPayed: number
    orderId: number
    // orderItems: 
    orderNumber: string
    payTime: any
    payType: any
    prodName: string
    productNums: number
    reduceAmount: any
    refundSts: number
    remarks: string
    shopId: number
    shopName: any
    status: number
    total: number
    updateTime: string
    // userAddrOrder: {addrOrderId: 2154, addrId: null, userId: null, receiver: '王先生', province: '台湾省', …}
    userId: string
}