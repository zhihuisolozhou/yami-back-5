import { RouteRecordRaw } from "vue-router";
export const getRoutes = (menulist: any) => {
    //最终返回的路由
    const routes: RouteRecordRaw[] = []

    //所有路由
    let allRoutes: RouteRecordRaw[] = []
    //获取router里面文件的路由
    const routeFiles = import.meta.globEager('../router/module/*.ts')
    console.log(routeFiles, '66');
    allRoutes = Object.keys(routeFiles).reduce((prev: any, next: any) => {
        {/* @ts-ignore  */ }
        prev.push(...routeFiles[next].default)
        return prev;
    }, [])
    const chaneRouer = (menu: any) => {
        for (const item of menu) {
            //判断type类型
            if (item.type === 1) {
                const route = allRoutes.find(val => val.path === '/' + item.url)
                if (route) routes.push(route)
            } else {
                chaneRouer(item.list)
            }
        }

    }
    chaneRouer(routeFiles)
    return routes;
}