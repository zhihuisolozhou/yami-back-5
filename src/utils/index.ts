//实例
import HttpRequset from "./requset";

const request = new HttpRequset({
    baseURL: import.meta.env.BASE_URL,
    timeout: 20000,
})
export default request

export const treeDataTranslate = (data: any, id = 'id', pid = 'parentId') => {
    let res = []
    let temp = []
    for (let i = 0; i < data.length; i++) {
        temp[data[i][id]] = data[i]
    }
    for (let j = 0; j < data.length; j++) {
        if (temp[data[j][pid]] && data[j][id] !== data[j][pid]) {
            if (!temp[data[j][pid]]['children']) {
                temp[data[j][pid]]['children'] = []
            }
            if (!temp[data[j][pid]]['_level']) {
                temp[data[j][pid]]['_level'] = 1
            }
            data[j]['_level'] = temp[data[j][pid]]._level + 1
            temp[data[j][pid]]['children'].push(data[j])
        } else {
            res.push(data[j])
        }
    }
    return res;
}