import { utils, writeFile, BookType } from "xlsx";

const useJsonToExcel = (options: { arr: any[], header: any, fileName: string, bookType: BookType }) => {
    //创建工作簿
    console.log(options.arr, 'options');

    const exSheer = utils.book_new();
    if (options.header) {
        options.arr = options.arr.map((item: any) => {
            const obj: any = {};
            for (const key in item) {
                // console.log(obj[options.header[key]], item[key],'99');
                obj[options.header[key]] = item[key]

            }
            console.log(obj, 'obj');
            return obj;
        })
    }
    //创建sheet页
    const ts = utils.json_to_sheet(options.arr);
    //把生成的工作表放到工作簿里面
    utils.book_append_sheet(exSheer, ts);
    //生成数据
    writeFile(exSheer, options.fileName, {
        bookType: options.bookType
    });

}
export default useJsonToExcel;