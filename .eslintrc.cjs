module.exports = {
    "env": {
        "browser": true,//运行到浏览器
        "es2021": true,//可以识别新语法
        "node":true,
        "vue/setup-compiler-macors":true,
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/vue3-essential",
        "plugin:@typescript-eslint/recommended",
        "plugin:prettier/recommended",
        "standard",
    ],
    "overrides": [
    ],
    "parser":'vue-eslint-parser',
    "parserOptions": {
        "ecmaVersion": "latest",
        "parser":'@typescript-eslint/parser',
        "sourceType": "module"
    },
    "plugins": [
        "vue",'@typescript-eslint'
    ],
    "rules": {
        semi:'off',
        'comma-dang':'off',
        'vue/multi-word-component-names':'off',
        '@typescript-eslint/no-var-requires':'off',
        // 'no-var':"error",
        // 'no-console':process.env.NOOD_ENV==='development'?'0':'2',
        // 'prettier/prettier':'error',
    }
}

//eslint-config-prettier  解决eselint中样式规范冲突时，以prettier为准
//eslint-plugin-prettier  将prettier作为eslint的规范来用

